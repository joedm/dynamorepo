﻿using System;
using Autofac;

namespace DynamoDbRepo.Testing
{
    public static class IocContainer
    {
        private static Lazy<IContainer> ContainerLazy => new Lazy<IContainer>(BuildContainer);

        public static IContainer Container => ContainerLazy.Value;

        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterModule<DynamoDbRepo.ContainerRegistrar>();

            return builder.Build();
        }

        public static TModule Resolve<TModule>()
        {
            return Container.Resolve<TModule>();
        }
    }
}