﻿using Amazon.DynamoDBv2.DataModel;

namespace DynamoDbRepo.Testing.Helper
{
    [DynamoDBTable("SampleTableWithTwoSecondaryIndex")]
    public class SampleClassWithTwoSecondaryIndex
    {
        [DynamoDBGlobalSecondaryIndexHashKey("SecondaryPartition-SecondarySort-Index")]
        public int SecondaryPartition { get; set; }

        [DynamoDBGlobalSecondaryIndexRangeKey("SecondaryPartition-SecondarySort-Index")]
        public int SecondarySort { get; set; }

        [DynamoDBGlobalSecondaryIndexHashKey("AnotherPartition-AnotherSort-Index")]
        public int AnotherPartition { get; set; }

        [DynamoDBGlobalSecondaryIndexRangeKey("AnotherPartition-AnotherSort-Index")]
        public int AnotherSort { get; set; }
    }
}