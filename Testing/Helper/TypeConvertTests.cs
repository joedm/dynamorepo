﻿using System;
using Amazon.DynamoDBv2.Model;
using DynamoDbRepo.Helper;
using Xunit;

// ReSharper disable InconsistentNaming

namespace DynamoDbRepo.Testing.Helper
{
    public class TypeConvertTests
    {
        #region TestWithNonNullValues
        [Fact]
        public void TestConveringSToChar()
        {
            var value = new AttributeValue { S = "c" };
            Assert.Equal(TypeConvert.SToChar(value), 'c');
        }

        [Fact]
        public void TestConveringSToNullableChar()
        {
            var value = new AttributeValue { S = "c" };
            Assert.Equal(TypeConvert.SToNullableChar(value), 'c');
        }

        [Fact]
        public void TestConveringSToString()
        {
            var value = new AttributeValue { S = "My String" };
            Assert.Equal(TypeConvert.SToString(value), "My String");
        }

        [Fact]
        public void TestConveringSToDateTime()
        {
            var value = new AttributeValue { S = "2008-09-22T02:50:00.0000000Z" };
            Assert.Equal(TypeConvert.SToDateTime(value), new DateTime(2008, 09, 22, 12, 50, 0));
        }

        [Fact]
        public void TestConveringSToNullableDateTime()
        {
            var value = new AttributeValue { S = "2008-09-22T02:50:00.0000000Z" };
            Assert.Equal(TypeConvert.SToNullableDateTime(value), new DateTime(2008, 09, 22, 12, 50, 0));
        }

        [Fact]
        public void TestConveringNToByte()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToByte(value), (byte)1);
        }

        [Fact]
        public void TestConveringNToNullableByte()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableByte(value), (byte)1);
        }

        [Fact]
        public void TestConveringNToInt32()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToInt32(value), (int)1);
        }

        [Fact]
        public void TestConveringNToNullableInt32()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableInt32(value), (int)1);
        }

        [Fact]
        public void TestConveringNToInt64()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToInt64(value), (long)1);
        }

        [Fact]
        public void TestConveringNToNullableInt64()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableInt64(value), (long)1);
        }

        [Fact]
        public void TestConveringNToUInt32()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToUInt32(value), (uint)1);
        }

        [Fact]
        public void TestConveringNToNullableUInt32()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableUInt32(value), (uint)1);
        }

        [Fact]
        public void TestConveringNToUInt64()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToUInt64(value), (ulong)1);
        }

        [Fact]
        public void TestConveringNToNullableUInt64()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableUInt64(value), (ulong)1);
        }

        [Fact]
        public void TestConveringNToDecimal()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToDecimal(value), (decimal)1);
        }

        [Fact]
        public void TestConveringNToNullableDecimal()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableDecimal(value), (decimal)1);
        }

        [Fact]
        public void TestConveringNToDouble()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToDouble(value), (double)1);
        }

        [Fact]
        public void TestConveringNToNullableDouble()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableDouble(value), (double)1);
        }

        [Fact]
        public void TestConveringNToFloat()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToFloat(value), (float)1);
        }

        [Fact]
        public void TestConveringNToNullableFloat()
        {
            var value = new AttributeValue { N = "1" };
            Assert.Equal(TypeConvert.NToNullableFloat(value), (float)1);
        }

        [Fact]
        public void TestConveringDynamoBoolToBool()
        {
            var value = new AttributeValue { BOOL = true };
            Assert.Equal(TypeConvert.BoolToBool(value), true);
        }

        [Fact]
        public void TestConveringBoolToNullableBool()
        {
            var value = new AttributeValue { BOOL = true };
            Assert.Equal(TypeConvert.BoolToNullableBool(value), true);
        }

        [Fact]
        public void TestConveringStringToS()
        {
            var value = "My String";
            Assert.Equal(TypeConvert.StringToS(value).S, "My String");
        }

        [Fact]
        public void TestConveringCharToS()
        {
            var value = 'c';
            Assert.Equal(TypeConvert.CharToS(value).S, "c");
        }

        [Fact]
        public void TestConveringNullableCharToS()
        {
            var value = 'c';
            Assert.Equal(TypeConvert.NullableCharToS(value).S, "c");
        }

        [Fact]
        public void TestConveringDateTimeToS()
        {
            var value = new DateTime(2008, 09, 22, 12, 50, 0);
            Assert.Equal(TypeConvert.DateTimeToS(value).S, "2008-09-22T02:50:00.0000000Z");
        }

        [Fact]
        public void TestConveringNullableDateTimeToS()
        {
            var value = new DateTime(2008, 09, 22, 12, 50, 0);
            Assert.Equal(TypeConvert.NullableDateTimeToS(value).S, "2008-09-22T02:50:00.0000000Z");
        }

        [Fact]
        public void TestConveringByteToN()
        {
            var value = byte.MaxValue;
            Assert.Equal(TypeConvert.ByteToN(value).N, byte.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableByteToN()
        {
            var value = byte.MaxValue;
            Assert.Equal(TypeConvert.NullableByteToN(value).N, byte.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringDecimalToN()
        {
            var value = decimal.MaxValue;
            Assert.Equal(TypeConvert.DecimalToN(value).N, decimal.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableDecimalToN()
        {
            var value = decimal.MaxValue;
            Assert.Equal(TypeConvert.NullableDecimalToN(value).N, decimal.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringDoubleToN()
        {
            var value = double.MaxValue;
            Assert.Equal(TypeConvert.DoubleToN(value).N, double.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableDoubleToN()
        {
            var value = double.MaxValue;
            Assert.Equal(TypeConvert.NullableDoubleToN(value).N, double.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringFloatToN()
        {
            var value = float.MaxValue;
            Assert.Equal(TypeConvert.FloatToN(value).N, float.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableFloatToN()
        {
            var value = float.MaxValue;
            Assert.Equal(TypeConvert.NullableFloatToN(value).N, float.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringInt32ToN()
        {
            var value = int.MaxValue;
            Assert.Equal(TypeConvert.Int32ToN(value).N, int.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableInt32ToN()
        {
            var value = int.MaxValue;
            Assert.Equal(TypeConvert.NullableInt32ToN(value).N, int.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringInt64ToN()
        {
            var value = long.MaxValue;
            Assert.Equal(TypeConvert.Int64ToN(value).N, long.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableInt64ToN()
        {
            var value = long.MaxValue;
            Assert.Equal(TypeConvert.NullableInt64ToN(value).N, long.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringUInt32ToN()
        {
            var value = uint.MaxValue;
            Assert.Equal(TypeConvert.UInt32ToN(value).N, uint.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableUInt32ToN()
        {
            var value = uint.MaxValue;
            Assert.Equal(TypeConvert.NullableUInt32ToN(value).N, uint.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringUInt64ToN()
        {
            var value = ulong.MaxValue;
            Assert.Equal(TypeConvert.UInt64ToN(value).N, ulong.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringNullableUInt64ToN()
        {
            var value = ulong.MaxValue;
            Assert.Equal(TypeConvert.NullableUInt64ToN(value).N, ulong.MaxValue.ToString());
        }

        [Fact]
        public void TestConveringBoolToBool()
        {
            var value = true;
            Assert.Equal(TypeConvert.BoolToBool(value).BOOL, true);
        }

        [Fact]
        public void TestConveringNullableBoolToBool()
        {
            var value = true;
            Assert.Equal(TypeConvert.NullableBoolToBool(value).BOOL, true);
        }
        #endregion
        #region TestWithNullValues
        [Fact]
        public void TestConveringSToCharWithNullThrowsException()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Throws<ArgumentNullException>(() => TypeConvert.SToChar(value));
        }

        [Fact]
        public void TestConveringSToNullableCharWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.SToNullableChar(value), null);
        }

        [Fact]
        public void TestConveringSToStringWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.SToString(value), null);
        }

        [Fact]
        public void TestConveringSToDateTimeWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.SToDateTime(value), DateTime.MinValue);
        }

        [Fact]
        public void TestConveringSToNullableDateTimeWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.SToNullableDateTime(value), null);
        }

        [Fact]
        public void TestConveringNToByteWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToByte(value), (byte)0);
        }

        [Fact]
        public void TestConveringNToNullableByteWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableByte(value), null);
        }

        [Fact]
        public void TestConveringNToInt32WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToInt32(value), (int)0);
        }

        [Fact]
        public void TestConveringNToNullableInt32WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableInt32(value), null);
        }

        [Fact]
        public void TestConveringNToInt64WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToInt64(value), (long)0);
        }

        [Fact]
        public void TestConveringNToNullableInt64WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableInt64(value), null);
        }

        [Fact]
        public void TestConveringNToUInt32WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToUInt32(value), (uint)0);
        }

        [Fact]
        public void TestConveringNToNullableUInt32WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableUInt32(value), null);
        }

        [Fact]
        public void TestConveringNToUInt64WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToUInt64(value), (ulong)0);
        }

        [Fact]
        public void TestConveringNToNullableUInt64WithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableUInt64(value), null);
        }

        [Fact]
        public void TestConveringNToDecimalWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToDecimal(value), (decimal)0);
        }

        [Fact]
        public void TestConveringNToNullableDecimalWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableDecimal(value), null);
        }

        [Fact]
        public void TestConveringNToDoubleWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToDouble(value), (double)0);
        }

        [Fact]
        public void TestConveringNToNullableDoubleWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableDouble(value), null);
        }

        [Fact]
        public void TestConveringNToFloatWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToFloat(value), (float)0);
        }

        [Fact]
        public void TestConveringNToNullableFloatWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.NToNullableFloat(value), null);
        }

        [Fact]
        public void TestConveringDynamoBoolToBoolWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.BoolToBool(value), false);
        }

        [Fact]
        public void TestConveringDynamoBoolToNullableBoolWithNull()
        {
            var value = new AttributeValue { NULL = true };
            Assert.Equal(TypeConvert.BoolToNullableBool(value), null);
        }

        [Fact]
        public void TestConveringStringToSWithNull()
        {
            Assert.True(TypeConvert.StringToS(null).NULL);
        }

        [Fact]
        public void TestConveringNullableCharToSWithNull()
        {
            Assert.True(TypeConvert.NullableCharToS(null).NULL);
        }

        [Fact]
        public void TestConveringNullableDateTimeToSWithNull()
        {
            Assert.True(TypeConvert.NullableDateTimeToS(null).NULL);
        }

        [Fact]
        public void TestConveringNullableByteToNWithNull()
        {
            Assert.True(TypeConvert.NullableByteToN(null).NULL);
        }

        [Fact]
        public void TestConveringNullableDecimalToNWithNull()
        {
            Assert.True(TypeConvert.NullableDecimalToN(null).NULL);
        }

        [Fact]
        public void TestConveringNullableDoubleToNWithNull()
        {
            Assert.True(TypeConvert.NullableDoubleToN(null).NULL);
        }

        [Fact]
        public void TestConveringNullableFloatToNWithNull()
        {
            Assert.True(TypeConvert.NullableFloatToN(null).NULL);
        }


        [Fact]
        public void TestConveringNullableInt32ToNWithNull()
        {
            Assert.True(TypeConvert.NullableInt32ToN(null).NULL);
        }

        [Fact]
        public void TestConveringNullableInt64ToNWithNull()
        {
            Assert.True(TypeConvert.NullableInt64ToN(null).NULL);
        }

        [Fact]
        public void TestConveringNullableUInt32ToNWithNull()
        {
            Assert.True(TypeConvert.NullableUInt32ToN(null).NULL);
        }

        [Fact]
        public void TestConveringNullableUInt64ToNWithNull()
        {
            Assert.True(TypeConvert.NullableUInt64ToN(null).NULL);
        }
        #endregion
    }
}