﻿using System;
using System.Reflection;
using DynamoDbRepo.Helper;
using Xunit;

namespace DynamoDbRepo.Testing.Helper
{
    public class PropertyMetadataTests
    {
        [Fact]
        public void TestGettingPropertyNameWithoutDifferentAttributeName()
        {
            var name = PropertyMetadata.GetAttributeName(typeof(SampleDynamoTable).GetProperty(nameof(SampleDynamoTable.MyString)));
            Assert.Equal(name, "MyString");
        }

        [Fact]
        public void TestGettingPropertyNameWithDifferentAttributeName()
        {
            var name = PropertyMetadata.GetAttributeName(typeof(SampleDynamoTable).GetProperty(nameof(SampleDynamoTable.SecondString)));
            Assert.Equal(name, "2ndString");
        }

        [Fact]
        public void TestGettingPropertyNameWithNoAttribute()
        {
            var name = PropertyMetadata.GetAttributeName(typeof(SampleDynamoTable).GetProperty(nameof(SampleDynamoTable.ThirdString)));
            Assert.Equal(name, "ThirdString");
        }

        [Fact]
        public void TestGettingPropertyNameWithoutDifferentAttributeNameT()
        {
            var name = PropertyMetadata.GetAttributeName<SampleDynamoTable>(nameof(SampleDynamoTable.MyString));
            Assert.Equal(name, "MyString");
        }

        [Fact]
        public void TestGettingPropertyNameWithDifferentAttributeNameT()
        {
            var name = PropertyMetadata.GetAttributeName<SampleDynamoTable>(nameof(SampleDynamoTable.SecondString));
            Assert.Equal(name, "2ndString");
        }

        [Fact]
        public void TestGettingPropertyNameWithNoAttributeT()
        {
            var name = PropertyMetadata.GetAttributeName<SampleDynamoTable>(nameof(SampleDynamoTable.ThirdString));
            Assert.Equal(name, "ThirdString");
        }

        [Theory]
        [InlineData("SecondaryPartition-SecondarySort-Index", "SecondaryPartition")]
        [InlineData("AnotherPartition-AnotherSort-Index", "AnotherPartition")]
        public void TestGetGlobalSecondaryIndexPartionKeyPropertyWithIndexNameT(string indexName, string expectedPropertyReturned)
        {
            PropertyInfo property = PropertyMetadata.GetGlobalSecondaryIndexPartitionKeyProperty<SampleClassWithTwoSecondaryIndex>(indexName);
            Assert.Equal(property.Name, expectedPropertyReturned);
        }

        [Fact]
        public void TestGetGlobalSecondaryIndexPartionKeyPropertyWithInvalidIndexNameThrowsExceptionT()
        {
            Assert.Throws<NullReferenceException>(() => PropertyMetadata.GetGlobalSecondaryIndexPartitionKeyProperty<SampleClassWithTwoSecondaryIndex>("Fake-Index-Name"));
        }

        [Theory]
        [InlineData("SecondaryPartition-SecondarySort-Index", "SecondaryPartition")]
        [InlineData("AnotherPartition-AnotherSort-Index", "AnotherPartition")]
        public void TestGetGlobalSecondaryIndexPartionKeyPropertyWithIndexName(string indexName, string expectedPropertyReturned)
        {
            PropertyInfo property = PropertyMetadata.GetGlobalSecondaryIndexPartitionKeyProperty(typeof(SampleClassWithTwoSecondaryIndex), indexName);
            Assert.Equal(property.Name, expectedPropertyReturned);
        }

        [Fact]
        public void TestGetGlobalSecondaryIndexPartionKeyPropertyWithInvalidIndexNameThrowsException()
        {
            Assert.Throws<NullReferenceException>(() => PropertyMetadata.GetGlobalSecondaryIndexPartitionKeyProperty(typeof(SampleClassWithTwoSecondaryIndex), "Fake-Index-Name"));
        }

        [Theory]
        [InlineData("SecondaryPartition-SecondarySort-Index", "SecondarySort")]
        [InlineData("AnotherPartition-AnotherSort-Index", "AnotherSort")]
        public void TestGetGlobalSecondaryIndexSortKeyPropertyWithIndexNameT(string indexName, string expectedPropertyReturned)
        {
            PropertyInfo property = PropertyMetadata.GetGlobalSecondaryIndexSortKeyProperty<SampleClassWithTwoSecondaryIndex>(indexName);
            Assert.Equal(property.Name, expectedPropertyReturned);
        }

        [Fact]
        public void TestGetGlobalSecondaryIndexSortKeyPropertyWithInvalidIndexNameThrowsExceptionT()
        {
            Assert.Throws<NullReferenceException>(() => PropertyMetadata.GetGlobalSecondaryIndexSortKeyProperty<SampleClassWithTwoSecondaryIndex>("Fake-Index-Name"));
        }

        [Theory]
        [InlineData("SecondaryPartition-SecondarySort-Index", "SecondarySort")]
        [InlineData("AnotherPartition-AnotherSort-Index", "AnotherSort")]
        public void TestGetGlobalSecondaryIndexSortKeyPropertyWithIndexName(string indexName, string expectedPropertyReturned)
        {
            PropertyInfo property = PropertyMetadata.GetGlobalSecondaryIndexSortKeyProperty(typeof(SampleClassWithTwoSecondaryIndex), indexName);
            Assert.Equal(property.Name, expectedPropertyReturned);
        }

        [Fact]
        public void TestGetGlobalSecondaryIndexSortKeyPropertyWithInvalidIndexNameThrowsException()
        {
            Assert.Throws<NullReferenceException>(() => PropertyMetadata.GetGlobalSecondaryIndexSortKeyProperty(typeof(SampleClassWithTwoSecondaryIndex), "Fake-Index-Name"));
        }

        [Fact]
        public void TestGetSecondaryIndexNameFromTableWithOnlyOneGlobalSecondaryIndexT()
        {
            var name = PropertyMetadata.GetGlobalSecondaryIndexName<SampleDynamoTable>();
            Assert.Equal(name, "SecondaryPartition-SecondarySort-Index");
        }

        [Fact]
        public void TestGetSecondaryIndexNameFromTableWithTwoGlobalSecondaryIndexThrowsExceptionT()
        {
            Assert.Throws<ArgumentException>(() => PropertyMetadata.GetGlobalSecondaryIndexName<SampleClassWithTwoSecondaryIndex>());
        }

        [Fact]
        public void TestGetSecondaryIndexNameFromTableWithOnlyOneGlobalSecondaryIndex()
        {
            var name = PropertyMetadata.GetGlobalSecondaryIndexName(typeof(SampleDynamoTable));
            Assert.Equal(name, "SecondaryPartition-SecondarySort-Index");
        }

        [Fact]
        public void TestGetSecondaryIndexNameFromTableWithTwoGlobalSecondaryIndexThrowsException()
        {
            Assert.Throws<ArgumentException>(() => PropertyMetadata.GetGlobalSecondaryIndexName(typeof(SampleClassWithTwoSecondaryIndex)));
        }

        [Fact]
        public void TestGetSecondaryIndexNameFromSecondaryIndexPartitionAttribute()
        {
            var name = PropertyMetadata.GetGlobalSecondaryIndexName(typeof(SampleDynamoTable).GetProperty(nameof(SampleDynamoTable.SecondaryPartition)));
            Assert.Equal(name, "SecondaryPartition-SecondarySort-Index");
        }

        [Fact]
        public void TestGetSecondaryIndexNameFromSecondaryIndexSortAttribute()
        {
            var name = PropertyMetadata.GetGlobalSecondaryIndexName(typeof(SampleDynamoTable).GetProperty(nameof(SampleDynamoTable.SecondarySort)));
            Assert.Equal(name, "SecondaryPartition-SecondarySort-Index");
        }

        [Fact]
        public void TestGettingTableNameFromType()
        {
            var name = PropertyMetadata.GetTableName<SampleDynamoTable>();
            Assert.Equal(name, "SampleTable");
        }

        [Fact]
        public void TestGettingTableNameFromTypeOf()
        {
            var name = PropertyMetadata.GetTableName(typeof(SampleDynamoTable));
            Assert.Equal(name, "SampleTable");
        }



        [Fact]
        public void PropertyWithDynamoDbIgnoreAttributeIsIgnored()
        {
            Assert.True(PropertyMetadata.IsIgnored(typeof(SampleDynamoTable).GetProperty("IgnoredProperty")));
        }

        [Fact]
        public void PropertyWithOutDynamoDbIgnoreAttributeIsNotIgnored()
        {
            Assert.False(PropertyMetadata.IsIgnored(typeof(SampleDynamoTable).GetProperty("MyString")));
        }
    }
}