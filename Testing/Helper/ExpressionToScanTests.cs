﻿using System;
using System.Linq;
using Amazon.DynamoDBv2.DocumentModel;
using DynamoDbRepo.Helper;
using Xunit;

// ReSharper disable NegativeEqualityExpression

namespace DynamoDbRepo.Testing.Helper
{
    public class ExpressionToScanTests
    {
        [Fact]
        public void TestTranslateSingleAndEqualExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString == "test");
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "test") == 1);
        }

        [Fact]
        public void TestTranslateSingleEqualNullExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString == null);
            Assert.True(result.Count(x => x.Operator == ScanOperator.IsNull && x.PropertyName == "MyString") == 1);
        }

        [Fact]
        public void TestTranslateSingleNotEqualNullExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString != null);
            Assert.True(result.Count(x => x.Operator == ScanOperator.IsNotNull && x.PropertyName == "MyString") == 1);
        }

        [Fact]
        public void TestTranslateSingleNotEqualExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString != "test");
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotEqual && x.PropertyName == "MyString" && (string)x.Values[0] == "test") == 1);
        }

        [Fact]
        public void TestTranslateSingleExpressionWithCustomAttributeName()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.SecondString == "test");
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "2ndString" && (string)x.Values[0] == "test") == 1);
        }

        [Fact]
        public void TestTranslateSingleGreaterThanExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.Number > 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.GreaterThan && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
        }

        [Fact]
        public void TestTranslateSingleGreaterThanOrEqualExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.Number >= 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.GreaterThanOrEqual && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
        }

        [Fact]
        public void TestTranslateSingleLessThanExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.Number < 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.LessThan && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
        }

        [Fact]
        public void TestTranslateSingleLessThanOrEqualExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.Number <= 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.LessThanOrEqual && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
        }

        [Fact]
        public void TestTranslateSingleExpressionWithPropertyOnRight()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => "Constant On Left" == x.MyString);
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Constant On Left") == 1);
        }

        [Fact]
        public void TestTranslateMultipleChainedAndAlsoExpressions()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => "Constant On Left" == x.MyString && x.MyString == "Constant On Right" && x.Number > 1 && x.ThirdString != "Third String");
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Constant On Left") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Constant On Right") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.GreaterThan && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotEqual && x.PropertyName == "ThirdString" && (string)x.Values[0] == "Third String") == 1);
        }

        [Fact]
        public void TestTranslateMultipleChainedAndAlsoExpressionsWithBraces()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => "Constant On Left" == x.MyString && (x.MyString == "Constant On Right" && x.Number > 1 && x.ThirdString != "Third String"));
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Constant On Left") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Constant On Right") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.GreaterThan && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotEqual && x.PropertyName == "ThirdString" && (string)x.Values[0] == "Third String") == 1);
        }

        [Fact]
        public void TestTranslateSingleStartsWithMethodExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString.StartsWith("The Start"));
            Assert.True(result.Count(x => x.Operator == ScanOperator.BeginsWith && x.PropertyName == "MyString" && (string)x.Values[0] == "The Start") == 1);
        }

        [Fact]
        public void TestTranslateSingleContainsMethodExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString.Contains("The Start"));
            Assert.True(result.Count(x => x.Operator == ScanOperator.Contains && x.PropertyName == "MyString" && (string)x.Values[0] == "The Start") == 1);
        }

        [Fact]
        public void TestTranslateSingleNotContainsMethodExpression()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => !x.MyString.Contains("The Start"));
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotContains && x.PropertyName == "MyString" && (string)x.Values[0] == "The Start") == 1);
        }

        [Fact]
        public void TestTranslateInvertingComparers()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x =>
                !x.MyString.Contains("Foo String1")
                && !(x.MyString == "Foo String2")
                && !(x.MyString != "Foo String3")
                && !(x.Number > 1)
                && !(x.Number >= 2)
                && !(x.Number < 3)
                && !(x.Number <= 4));
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotContains && x.PropertyName == "MyString" && (string)x.Values[0] == "Foo String1") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotEqual && x.PropertyName == "MyString" && (string)x.Values[0] == "Foo String2") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Foo String3") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.LessThanOrEqual && x.PropertyName == "Number" && (int)x.Values[0] == 1) == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.LessThan && x.PropertyName == "Number" && (int)x.Values[0] == 2) == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.GreaterThanOrEqual && x.PropertyName == "Number" && (int)x.Values[0] == 3) == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.GreaterThan && x.PropertyName == "Number" && (int)x.Values[0] == 4) == 1);
        }

        [Fact]
        public void TestStandardCompareMixedWithInvertedCompare()
        {
            var result = ExpressionToScan.Translate<SampleDynamoTable>(x => x.MyString == "Foo 1" && !(x.SecondString == "Foo 2"));
            Assert.True(result.Count(x => x.Operator == ScanOperator.Equal && x.PropertyName == "MyString" && (string)x.Values[0] == "Foo 1") == 1);
            Assert.True(result.Count(x => x.Operator == ScanOperator.NotEqual && x.PropertyName == "2ndString" && (string)x.Values[0] == "Foo 2") == 1);
        }

        [Fact]
        public void TestExpressionWithOrElseThrowsNotSupportedException()
        {
            Assert.Throws<NotSupportedException>(() => ExpressionToScan.Translate<SampleDynamoTable>(x => "Constant On Left" == x.MyString || x.MyString == "Constant On Right"));
        }

        [Fact]
        public void TestMultipleComparersInSingleInvertThrowsNotSupportedException()
        {
            Assert.Throws<NotSupportedException>(() => ExpressionToScan.Translate<SampleDynamoTable>(x => !(x.MyString == "Foo String2" && x.MyString == "Foo String3")));
        }
    }
}