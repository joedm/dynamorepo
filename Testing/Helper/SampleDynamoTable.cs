﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace DynamoDbRepo.Testing.Helper
{
    [DynamoDBTable("SampleTable")]
    public class SampleDynamoTable
    {
        [DynamoDBProperty]
        public string MyString { get; set; }

        [DynamoDBProperty("2ndString")]
        public string SecondString { get; set; }

        public string ThirdString { get; set; }

        public int Number { get; set; }

        [DynamoDBGlobalSecondaryIndexHashKey("SecondaryPartition-SecondarySort-Index")]
        public int SecondaryPartition { get; set; }

        [DynamoDBGlobalSecondaryIndexRangeKey("SecondaryPartition-SecondarySort-Index")]
        public int SecondarySort { get; set; }

        public string SampleString { get; set; }

        public char SampleChar { get; set; }

        public DateTime SampleDateTime { get; set; }

        public int SampleInt32 { get; set; }

        public long SampleInt64 { get; set; }

        public uint SampleUInt32 { get; set; }

        public ulong SampleUInt64 { get; set; }

        public decimal SampleDecimal { get; set; }

        public double SampleDouble { get; set; }

        public float SampleFloat { get; set; }

        public bool SampleBool { get; set; }

        [DynamoDBIgnore]
        public string IgnoredProperty { get; set; }
    }
}