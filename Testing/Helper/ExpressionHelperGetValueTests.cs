﻿using System;
using System.Linq.Expressions;
using DynamoDbRepo.Helper;
using Xunit;

namespace DynamoDbRepo.Testing.Helper
{
    public class ExpressionHelperGetValueTests
    {
        // sample lambda expression
        Func<string, string> _sampleLambda = s => $"Input: {s}";

        // sample property
        string SampleProperty { get { return "Property Return"; } }

        // sample method call expression
        string GetSample()
        {
            return "Method Return";
        }

        string GetSample(string s)
        {
            return $"Method Parameter: {s}";
        }

        private string _memberString = "Member String";

        public Expression GetExpression<T>(Expression<Func<T>> function)
        {
            return (MethodCallExpression) function.Body;
        }

        [Fact]
        public void TestGetMethodReturnValueWithLambdaParameter()
        {
            Expression expression = GetExpression(() => GetSample(_sampleLambda("Hello World")));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World");
        }

        [Fact]
        public void TestGetMethodReturnValueWithPropertyParameter()
        {
            Expression expression = GetExpression(() => GetSample(SampleProperty));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return");
        }

        [Fact]
        public void TestGetMethodReturnValueWithMethodParameter()
        {
            Expression expression = GetExpression(() => GetSample(GetSample()));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return");
        }

        [Fact]
        public void TestGetMethodReturnValueWithConstantParameter()
        {
            Expression expression = GetExpression(() => GetSample("Constant String"));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Constant String");
        }

        [Fact]
        public void TestGetMethodReturnValueWithMemberParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(_memberString));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithLambdaParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(_sampleLambda("Hello World")));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithPropertyParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(SampleProperty));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithMethodParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(GetSample()));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithConstantParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample("Constant String"));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Constant String");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithMemberParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(_memberString));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String");
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalLambdaParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.SampleLambda("Hello World")));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World");
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalPropertyParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.SampleProperty));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return");
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalMethodParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.GetSample()));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return");
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalMemberParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.MemberString));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalLambdaParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.SampleLambda("Hello World")));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalPropertyParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.SampleProperty));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalMethodParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.GetSample()));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return");
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalMemberParameter()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.MemberString));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String");
        }

        [Fact]
        public void TestGetMethodReturnValueWithLambdaParameterWithExpressionChain()
        {
            Expression expression = GetExpression(() => GetSample(_sampleLambda("Hello World").ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithPropertyParameterWithExpressionChain()
        {
            Expression expression = GetExpression(() => GetSample(SampleProperty.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithMethodParameterWithExpressionChain()
        {
            Expression expression = GetExpression(() => GetSample(GetSample().ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithConstantParameterWithExpressionChain()
        {
            Expression expression = GetExpression(() => GetSample("Constant String".ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Constant String".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithMemberParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(_memberString.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithLambdaParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(_sampleLambda("Hello World").ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithPropertyParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(SampleProperty.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithMethodParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(GetSample().ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithConstantParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample("Constant String".ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Constant String".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithMemberParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(_memberString.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalLambdaParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.SampleLambda("Hello World").ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalPropertyParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.SampleProperty.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalMethodParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.GetSample().ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return".ToUpper());
        }

        [Fact]
        public void TestGetMethodReturnValueWithExternalMemberParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => GetSample(externalClass.MemberString.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalLambdaParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.SampleLambda("Hello World").ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Input: Hello World".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalPropertyParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.SampleProperty.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Property Return".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalMethodParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.GetSample().ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Method Return".ToUpper());
        }

        [Fact]
        public void TestExternalGetMethodReturnValueWithExternalMemberParameterWithExpressionChain()
        {
            SampleExternalClass externalClass = new SampleExternalClass();
            Expression expression = GetExpression(() => externalClass.GetSample(externalClass.MemberString.ToUpper()).ToUpper());
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: Member String".ToUpper());
        }

        [Fact]
        public void TestConstructNewInstanceOfDateTime()
        {
            Expression expression = GetExpression(() => GetSample(new DateTime(1900, 01,01).ToString("yyyy MM dd")));
            string result = (string)ExpressionHelper.GetReturnValue(expression);
            Assert.Equal(result, "Method Parameter: 1900 01 01");
        }
    }

    public class SampleExternalClass
    {
        public string MemberString = "Member String";
        public Func<string, string> SampleLambda = s => $"Input: {s}";

        // sample property
        public string SampleProperty { get { return "Property Return"; } }

        // sample method call expression
        public string GetSample()
        {
            return "Method Return";
        }

        public string GetSample(string s)
        {
            return $"Method Parameter: {s}";
        }
    }
}
