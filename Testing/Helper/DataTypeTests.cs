﻿using System;
using Amazon.DynamoDBv2.Model;
using DynamoDbRepo.Helper;
using Xunit;

namespace DynamoDbRepo.Testing.Helper
{
    public class DataTypeTests
    {
        [Theory]
        [InlineData(typeof(string), "Sample String")]
        [InlineData(typeof(char), 'c')]
        [InlineData(typeof(char?), 'c')]
        public void TestGetStringAttributeValueUsingType(Type type, object value)
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(type, value);
            Assert.Equal(attributeValue.S, value.ToString());
        }

        [Fact]
        public void TestGetDateTimeAttributeValueUsingType()
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(DateTime), new DateTime(1900, 01, 01));
            Assert.Equal(attributeValue.S, new DateTime(1900, 01, 01).ToUniversalTime().ToString("o"));
        }

        [Theory]
        [InlineData(typeof(int), int.MaxValue)]
        [InlineData(typeof(long), long.MaxValue)]
        [InlineData(typeof(uint), uint.MaxValue)]
        [InlineData(typeof(ulong), ulong.MaxValue)]
        [InlineData(typeof(double), double.MaxValue)]
        [InlineData(typeof(float), float.MaxValue)]
        [InlineData(typeof(int?), int.MaxValue)]
        [InlineData(typeof(long?), long.MaxValue)]
        [InlineData(typeof(uint?), uint.MaxValue)]
        [InlineData(typeof(ulong?), ulong.MaxValue)]
        [InlineData(typeof(double?), double.MaxValue)]
        [InlineData(typeof(float?), float.MaxValue)]
        public void TestGetNumberAttributeValueUsingType(Type type, object value)
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(type, value);
            Assert.Equal(attributeValue.N, value.ToString());
        }

        [Theory]
        [InlineData(typeof(string))]
        [InlineData(typeof(char?))]
        [InlineData(typeof(DateTime?))]
        [InlineData(typeof(int?))]
        [InlineData(typeof(long?))]
        [InlineData(typeof(uint?))]
        [InlineData(typeof(ulong?))]
        [InlineData(typeof(decimal?))]
        [InlineData(typeof(double?))]
        [InlineData(typeof(float?))]
        [InlineData(typeof(bool?))]
        public void TestGetNumberAttributeValueAsNullUsingType(Type type)
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(type, null);
            Assert.True(attributeValue.NULL);
        }

        [Fact] // A test to itself because you can't use a decimal in xUnit InLine data.
        public void TestGetDecimalAttributeValueUsingType()
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(decimal), decimal.MaxValue);
            Assert.Equal(attributeValue.N, decimal.MaxValue.ToString());
        }

        [Fact]
        public void TestGetBoolAttributeValueUsingType()
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(bool), true);
            Assert.Equal(attributeValue.BOOL, true);
        }

        [Theory]
        [InlineData(nameof(SampleDynamoTable.SampleString), "Sample String")]
        [InlineData(nameof(SampleDynamoTable.SampleChar), 'c')]
        public void TestGetStringAttributeValueUsingPropertyInfo(string propertyName, object value)
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(SampleDynamoTable).GetProperty(propertyName), value);
            Assert.Equal(attributeValue.S, value.ToString());
        }

        [Fact]
        public void TestGetDateTimeAttributeValueUsingPropertyInfo()
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(SampleDynamoTable).GetProperty("SampleDateTime"), new DateTime(1900, 01, 01));
            Assert.Equal(attributeValue.S, new DateTime(1900, 01, 01).ToUniversalTime().ToString("o"));
        }

        [Theory]
        [InlineData(nameof(SampleDynamoTable.SampleInt32), int.MaxValue)]
        [InlineData(nameof(SampleDynamoTable.SampleInt64), long.MaxValue)]
        [InlineData(nameof(SampleDynamoTable.SampleUInt32), uint.MaxValue)]
        [InlineData(nameof(SampleDynamoTable.SampleUInt64), ulong.MaxValue)]
        [InlineData(nameof(SampleDynamoTable.SampleDouble), double.MaxValue)]
        [InlineData(nameof(SampleDynamoTable.SampleFloat), float.MaxValue)]
        public void TestGetNumberAttributeValueUsingPropertyInfo(string propertyName, object value)
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(SampleDynamoTable).GetProperty(propertyName), value);
            Assert.Equal(attributeValue.N, value.ToString());
        }

        [Fact] // A test to itself because you can't use a decimal in xUnit InLine data.
        public void TestGetDecimalAttributeValueUsingPropertyInfo()
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(SampleDynamoTable).GetProperty("SampleDecimal"), decimal.MaxValue);
            Assert.Equal(attributeValue.N, decimal.MaxValue.ToString());
        }

        [Fact]
        public void TestGetBoolAttributeValueUsingPropertyInfo()
        {
            AttributeValue attributeValue = DataType.GetAttributeValueFromObject(typeof(SampleDynamoTable).GetProperty("SampleBool"), true);
            Assert.Equal(attributeValue.BOOL, true);
        }

        [Theory]
        [InlineData(typeof(string))]
        [InlineData(typeof(char))]
        [InlineData(typeof(DateTime))]
        [InlineData(typeof(int))]
        [InlineData(typeof(long))]
        [InlineData(typeof(uint))]
        [InlineData(typeof(ulong))]
        [InlineData(typeof(decimal))]
        [InlineData(typeof(double))]
        [InlineData(typeof(float))]
        [InlineData(typeof(bool))]
        [InlineData(typeof(char?))]
        [InlineData(typeof(DateTime?))]
        [InlineData(typeof(int?))]
        [InlineData(typeof(long?))]
        [InlineData(typeof(uint?))]
        [InlineData(typeof(ulong?))]
        [InlineData(typeof(decimal?))]
        [InlineData(typeof(double?))]
        [InlineData(typeof(float?))]
        [InlineData(typeof(bool?))]
        public void TestGetTypeMapForPrimativeTypes(Type type)
        {
            Assert.NotNull(DataType.GetTypeMap(type));
        }

        [Fact]
        public void TestGetTypeMapForNotSupportedTypeThrowsException()
        {
            Assert.Throws<NotSupportedException>(() => DataType.GetTypeMap(typeof(void)));
        }
    }
}