﻿using Amazon.DynamoDBv2.DocumentModel;
using DynamoDbRepo.Helper;
using Xunit;

namespace DynamoDbRepo.Testing.Helper
{
    public class QueryOperatorTests
    {
        [Theory]
        [InlineData(QueryOperator.Equal, "=")]
        [InlineData(QueryOperator.LessThanOrEqual, "<=")]
        [InlineData(QueryOperator.LessThan, "<")]
        [InlineData(QueryOperator.GreaterThanOrEqual, ">=")]
        [InlineData(QueryOperator.GreaterThan, ">")]
        [InlineData(QueryOperator.BeginsWith, "begins_with")]
        [InlineData(QueryOperator.Between, "BETWEEN")]
        public void TestConvertingHighLevelOperatorEnumToLowLevelDynamoQueryOperatorReturnsCorrectString(QueryOperator queryOperator, string expectedResult)
        {
            Assert.Equal(QueryHelper.HighLevelOperatorToLowLevelOperator(queryOperator), expectedResult);
        }
    }
}