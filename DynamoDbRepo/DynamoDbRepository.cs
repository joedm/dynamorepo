﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using DynamoDbRepo.Helper;
using DynamoDbRepo.Model;
using DynamoDBContextConfig = Amazon.DynamoDBv2.DataModel.DynamoDBContextConfig;

namespace DynamoDbRepo
{
    public abstract class DynamoDbRepository<T> : IDynamoDbRepository<T> where T : class
    {
        public AmazonDynamoDBClient Client { get; set; }
        public DynamoDBContext Context { get; set; }
        private ICollection<PropertyMap> PropertyMappings { get; }

        protected DynamoDbRepository(IDynamoDbClient client)
        {
            if (client == null) throw new ArgumentNullException(nameof(client));

            Client = client.Client;
            Context = new DynamoDBContext(client.Client);

            PropertyMappings = CreatePropertyMappings();
        }

        public virtual T GetByHashWithNoSortKey(object hashKey)
        {
            return Context.Load<T>(hashKey, new DynamoDBOperationConfig { ConsistentRead = true });
        }

        public virtual T GetByHashWithSortKey(object hashKey, object sortKey)
        {
            return Context.Load<T>(hashKey, sortKey, new DynamoDBOperationConfig { ConsistentRead = true });
        }

        public virtual IEnumerable<T> ScanBy(Expression<Func<T, bool>> predicate)
        {
            ICollection<ScanCondition> scanConditions = ExpressionToScan.Translate<T>(predicate);
            return ScanBy(scanConditions.ToArray());
        }

        public virtual IEnumerable<T> ScanBy(params ScanCondition[] conditions)
        {
            return Context.Scan<T>(conditions);
        }

        public virtual IEnumerable<T> QueryTableWithNoSortKey(object hashKey)
        {
            return Context.Query<T>(hashKey);
        }

        public virtual IEnumerable<T> QueryTableWithSortKey(object hashKey, QueryOperator queryOperator, params object[] values)
        {
            ThrowExceptionIfValuesLengthDontMatchQueryOperator(queryOperator, values.Length);
            return Context.Query<T>(hashKey, queryOperator, values);
        }

        public virtual IEnumerable<T> QueryGlobalSecondaryIndexWithNoSortKey(object hashKey)
        {
            string indexName = PropertyMetadata.GetGlobalSecondaryIndexName<T>();
            return QueryGlobalSecondaryIndexWithNoSortKey(indexName, hashKey);
        }

        public virtual IEnumerable<T> QueryGlobalSecondaryIndexWithNoSortKey(string indexName, object hashKey)
        {
            PropertyInfo partitionKeyProperty = PropertyMetadata.GetGlobalSecondaryIndexPartitionKeyProperty<T>(indexName);
            string partitionKeyName = GetAttributeNameUsingPropertyMap(partitionKeyProperty);

            QueryRequest queryRequest = new QueryRequest
            {
                TableName = PropertyMetadata.GetTableName<T>(),
                IndexName = indexName,
                KeyConditionExpression = $"#{partitionKeyName} = :v_{partitionKeyName}",
                ExpressionAttributeNames = new Dictionary<string, string> { { $"#{partitionKeyName}", partitionKeyName } },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue> { { $":v_{partitionKeyName}", GetAttributeValueUsingPropertyMap(partitionKeyProperty, hashKey) } },
                ScanIndexForward = true
            };

            QueryResponse result = Client.Query(queryRequest);

            return result.Items.Select(ConstructModelFromResponseItem);
        }

        public virtual IEnumerable<T> QueryGlobalSecondaryIndexWithSortKey(object partitionKey, QueryOperator queryOperator, params object[] values)
        {
            string indexName = PropertyMetadata.GetGlobalSecondaryIndexName<T>();
            return QueryGlobalSecondaryIndexWithSortKey(indexName, partitionKey, queryOperator, values);
        }

        public virtual IEnumerable<T> QueryGlobalSecondaryIndexWithSortKey(string indexName, object partitionKey, QueryOperator queryOperator, params object[] values)
        {
            ThrowExceptionIfValuesLengthDontMatchQueryOperator(queryOperator, values.Length);
            PropertyInfo partitionKeyProperty = PropertyMetadata.GetGlobalSecondaryIndexPartitionKeyProperty<T>(indexName);
            PropertyInfo sortKeyProperty = PropertyMetadata.GetGlobalSecondaryIndexSortKeyProperty<T>(indexName);
            string partitionKeyName = GetAttributeNameUsingPropertyMap(partitionKeyProperty);
            string sortKeyName = GetAttributeNameUsingPropertyMap(sortKeyProperty);
            string lowLvlQueryOperator = QueryHelper.HighLevelOperatorToLowLevelOperator(queryOperator);

            string keyConditionExpression = $"#{partitionKeyName} = :v_{partitionKeyName} and #{sortKeyName} {lowLvlQueryOperator} :v_{sortKeyName}";
            var expressionAttributeValues = new Dictionary<string, AttributeValue>
            {
                {$":v_{partitionKeyName}", GetAttributeValueUsingPropertyMap(partitionKeyProperty, partitionKey)},
                {$":v_{sortKeyName}", GetAttributeValueUsingPropertyMap(sortKeyProperty, values[0])}
            };

            if (queryOperator == QueryOperator.Between)
            {
                keyConditionExpression += $" and :v_{sortKeyName}2";
                expressionAttributeValues.Add($":v_{sortKeyName}2", GetAttributeValueUsingPropertyMap(sortKeyProperty, values[1]));
            }

            QueryRequest queryRequest = new QueryRequest
            {
                TableName = PropertyMetadata.GetTableName<T>(),
                IndexName = indexName,
                KeyConditionExpression = keyConditionExpression,
                ExpressionAttributeNames = new Dictionary<string, string>
                {
                    { $"#{partitionKeyName}", partitionKeyName },
                    { $"#{sortKeyName}", sortKeyName }
                },
                ExpressionAttributeValues = expressionAttributeValues,
                ScanIndexForward = true
            };

            QueryResponse result = Client.Query(queryRequest);

            return result.Items.Select(ConstructModelFromResponseItem);
        }

        public virtual void Delete(T entity)
        {
            Context.Delete<T>(entity, new DynamoDBContextConfig { ConsistentRead = true });
        }

        public virtual void Save(T entity)
        {
            Context.Save(entity);
        }
        protected virtual T ConstructNewModelInstance()
        {
            return (T)Activator.CreateInstance(typeof(T));
        }

        protected AttributeValue GetAttributeValueUsingPropertyMap(PropertyInfo propertyInfo, object value)
        {
            TypeMap typeMap = GetPropertyMap(propertyInfo.Name).TypeMap;
            return typeMap.ToDynamoAttributeValue(value);
        }

        protected string GetAttributeNameUsingPropertyMap(PropertyInfo propertyInfo)
        {
            return GetPropertyMap(propertyInfo.Name).DynamoAttributeName;
        }

        protected object GetPropertyObjectFromQueryResponseItem(string propertyName, Dictionary<string, AttributeValue> queryResponseItem)
        {
            PropertyMap map = GetPropertyMap(propertyName);
            AttributeValue value = queryResponseItem.First(x => x.Key == map.DynamoAttributeName).Value;
            if (value == null)
                throw new NullReferenceException();
            return map.TypeMap.ToDotNetObject(value);
        }

        protected T ConstructModelFromResponseItem(Dictionary<string, AttributeValue> queryResponseItem)
        {
            T result = ConstructNewModelInstance();
            foreach (PropertyMap propertyMap in PropertyMappings)
            {
                AttributeValue dynamoValue = queryResponseItem.FirstOrDefault(x => x.Key == propertyMap.DynamoAttributeName).Value;
                object value = propertyMap.TypeMap.ToDotNetObject(dynamoValue);
                propertyMap.PropertyInfo.SetValue(result, value);
            }

            return result;
        }

        private ICollection<PropertyMap> CreatePropertyMappings()
        {
            ICollection<PropertyMap> result = new List<PropertyMap>();

            foreach (PropertyInfo property in typeof(T).GetProperties())
            {
                if (PropertyMetadata.IsIgnored(property))
                    continue;

                result.Add(new PropertyMap
                {
                    PropertyName = property.Name,
                    PropertyInfo = property,
                    DynamoAttributeName = PropertyMetadata.GetAttributeName(property),
                    TypeMap = DataType.GetTypeMap(property.PropertyType)
                });
            }

            return result;
        }

        private PropertyMap GetPropertyMap(string propertyName)
        {
            PropertyMap map = PropertyMappings.First(x => x.PropertyName == propertyName);
            if (map == null)
                throw new NullReferenceException($"Could not locate {nameof(PropertyMap)} for {propertyName}");

            return map;
        }

        private void ThrowExceptionIfValuesLengthDontMatchQueryOperator(QueryOperator queryOperator, int length)
        {
            if (queryOperator == QueryOperator.Between && length != 2)
                throw new ArgumentOutOfRangeException($"Query Operator {QueryOperator.Between} must be supplied with exactly 2 compare values.");
            if (queryOperator != QueryOperator.Between && length != 1)
                throw new ArgumentOutOfRangeException($"Query Operator {queryOperator} must be supplied with exactly 1 compare value.");
        }
    }
}
