﻿using DynamoDbRepo.Model.Sample;

namespace DynamoDbRepo.Repository
{
    public class SmsFromApiLogRepository : DynamoDbRepository<SmsFromApiLog>, ISmsFromApiLogRepository
    {
        public SmsFromApiLogRepository(IDynamoDbClient client) : base(client)
        {
        }
    }
}