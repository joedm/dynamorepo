﻿using DynamoDbRepo.Model.Sample;

namespace DynamoDbRepo.Repository
{
    public interface ISmsFromApiReplyRepository : IDynamoDbRepository<SmsFromApiReply>
    {
    }
}