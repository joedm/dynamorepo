﻿using DynamoDbRepo.Model.Sample;

namespace DynamoDbRepo.Repository
{
    public class SmsFromApiReplyRepository : DynamoDbRepository<SmsFromApiReply>, ISmsFromApiReplyRepository
    {
        public SmsFromApiReplyRepository(IDynamoDbClient client) : base(client)
        {
        }
    }
}