﻿using DynamoDbRepo.Model.Sample;

namespace DynamoDbRepo.Repository
{
    public interface ISmsFromApiLogRepository : IDynamoDbRepository<SmsFromApiLog>
    {
    }
}