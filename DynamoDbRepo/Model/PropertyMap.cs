﻿using System.Reflection;

namespace DynamoDbRepo.Model
{
    public class PropertyMap
    {
        public string PropertyName { get; set; }
        public PropertyInfo PropertyInfo { get; set; }
        public string DynamoAttributeName { get; set; }
        public TypeMap TypeMap { get; set; }
    }
}