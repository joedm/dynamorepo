﻿using System;
using Amazon.DynamoDBv2.Model;

namespace DynamoDbRepo.Model
{
    public class TypeMap
    {
        public TypeMap(Type dotNetType, string dynamoType, Func<AttributeValue, object> toDotNetObject, Func<object, AttributeValue> toDynamoAttributeValue)
        {
            DotNetType = dotNetType;
            DynamoType = dynamoType;
            ToDotNetObject = toDotNetObject;
            ToDynamoAttributeValue = toDynamoAttributeValue;
        }

        public Type DotNetType { get; set; }
        public string DynamoType { get; set; }
        public Func<AttributeValue, object> ToDotNetObject { get; set; }
        public Func<object, AttributeValue> ToDynamoAttributeValue { get; set; }
    }
}