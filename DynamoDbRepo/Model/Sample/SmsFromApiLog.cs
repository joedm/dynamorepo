﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace DynamoDbRepo.Model.Sample
{
    [DynamoDBTable("SmsFromApiLogs")]
    public class SmsFromApiLog
    {
        [DynamoDBHashKey]
        public long MessageId { get; set; }

        [DynamoDBGlobalSecondaryIndexHashKey("PartnerCid-CreateDate-Index")]
        public int PartnerCid { get; set; }

        [DynamoDBGlobalSecondaryIndexRangeKey("PartnerCid-CreateDate-Index")]
        public DateTime CreateDate { get; set; }

        [DynamoDBProperty]
        public string MobileNumber { get; set; }

        [DynamoDBProperty]
        public string Message { get; set; }

        [DynamoDBProperty]
        public SmsReplyType ReplyType { get; set; }

        [DynamoDBProperty]
        public string ReplyAddress { get; set; }

        [DynamoDBVersion]
        public int? Version { get; set; }
    }
}