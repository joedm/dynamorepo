﻿namespace DynamoDbRepo.Model.Sample
{
    public enum SmsReplyType
    {
        NoReply,
        Email,
        Url
    }
}