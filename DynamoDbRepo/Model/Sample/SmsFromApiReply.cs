﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace DynamoDbRepo.Model.Sample
{
    [DynamoDBTable("SmsFromApiReplies")]
    public class SmsFromApiReply
    {
        [DynamoDBHashKey]
        public long MessageId { get; set; }

        [DynamoDBRangeKey]
        public DateTime CreateDate { get; set; }

        [DynamoDBProperty]
        public string Message { get; set; }

        [DynamoDBVersion]
        public int? Version { get; set; }
    }
}