﻿using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using Amazon;
using Amazon.Runtime;
using Autofac;
using Autofac.Core;

namespace DynamoDbRepo
{
    public class ContainerRegistrar : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var dataAccess = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(dataAccess)
                   .Where(t => t.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterType<DynamoDbClient>().As<IDynamoDbClient>().WithParameters(new List<Parameter>
            {
                new NamedParameter("credentials", new BasicAWSCredentials(ConfigurationManager.AppSettings["DynamoDb:AccessKey"], ConfigurationManager.AppSettings["DynamoDb:SecretKey"])),
                new NamedParameter("region", RegionEndpoint.APSoutheast2)
            }).SingleInstance();
        }
    }
}
