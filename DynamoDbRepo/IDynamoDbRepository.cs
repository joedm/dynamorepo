﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace DynamoDbRepo
{
    public interface IDynamoDbRepository<T> where T : class
    {
        T GetByHashWithNoSortKey(object hashKey);
        T GetByHashWithSortKey(object hashKey, object sortKey);
        IEnumerable<T> ScanBy(Expression<Func<T, bool>> predicate);
        IEnumerable<T> ScanBy(params ScanCondition[] conditions);
        IEnumerable<T> QueryTableWithNoSortKey(object partitionKey);
        IEnumerable<T> QueryTableWithSortKey(object partitionKey, QueryOperator queryOperator, params object[] values);
        IEnumerable<T> QueryGlobalSecondaryIndexWithNoSortKey(object hashKey);
        IEnumerable<T> QueryGlobalSecondaryIndexWithNoSortKey(string indexName, object hashKey);
        IEnumerable<T> QueryGlobalSecondaryIndexWithSortKey(object hashKey, QueryOperator queryOperator, params object[] values);
        IEnumerable<T> QueryGlobalSecondaryIndexWithSortKey(string indexName, object hashKey, QueryOperator queryOperator, params object[] values);
        void Delete(T entity);
        void Save(T entity);
    }
}