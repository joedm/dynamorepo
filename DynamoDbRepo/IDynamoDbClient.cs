﻿using Amazon.DynamoDBv2;

namespace DynamoDbRepo
{
    public interface IDynamoDbClient
    {
        AmazonDynamoDBClient Client { get; }
    }
}