﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.Runtime;

namespace DynamoDbRepo
{
    public class DynamoDbClient : IDynamoDbClient
    {
        public AmazonDynamoDBClient Client { get; }

        public DynamoDbClient(AWSCredentials credentials, RegionEndpoint region)
        {
            Client = new AmazonDynamoDBClient(credentials, region);
        }
    }
}