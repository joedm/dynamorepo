﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DynamoDbRepo.Helper
{
    public static class AttributeHelper
    {
        public static IEnumerable<PropertyInfo> FindPropertiesWithAttribute(Type classToSearch, Type attributeToFind)
        {
            return classToSearch.GetProperties().Where(property => property.CustomAttributes.Any(x => x.AttributeType == attributeToFind));
        }
    }
}