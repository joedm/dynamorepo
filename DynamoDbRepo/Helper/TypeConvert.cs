﻿using System;
using System.Linq;
using Amazon.DynamoDBv2.Model;

// ReSharper disable InconsistentNaming

namespace DynamoDbRepo.Helper
{
    public static class TypeConvert
    {
        #region FromDynamoToDotNetMethods
        public static object SToChar(AttributeValue value)
        {
            return value.S.First();
        }

        public static object SToNullableChar(AttributeValue value)
        {
            return value.NULL ? null : (char?)value.S.FirstOrDefault();
        }

        public static object SToString(AttributeValue value)
        {
            return value.NULL ? null : value.S;
        }

        public static object SToDateTime(AttributeValue value)
        {
            return value.NULL ? DateTime.MinValue : Convert.ToDateTime(value.S);
        }

        public static object SToNullableDateTime(AttributeValue value)
        {
            return value.NULL ? null : (DateTime?)Convert.ToDateTime(value.S);
        }

        public static object NToByte(AttributeValue value)
        {
            return value.NULL ? (byte)0 : Convert.ToByte(value.N);
        }

        public static object NToNullableByte(AttributeValue value)
        {
            return value.NULL ? null : (byte?)Convert.ToByte(value.N);
        }

        public static object NToInt32(AttributeValue value)
        {
            return value.NULL ? (int)0 : Convert.ToInt32(value.N);
        }

        public static object NToNullableInt32(AttributeValue value)
        {
            return value.NULL ? null : (int?)Convert.ToInt32(value.N);
        }

        public static object NToInt64(AttributeValue value)
        {
            return value.NULL ? (long)0 : Convert.ToInt64(value.N);
        }

        public static object NToNullableInt64(AttributeValue value)
        {
            return value.NULL ? null : (long?)Convert.ToInt64(value.N);
        }

        public static object NToUInt32(AttributeValue value)
        {
            return value.NULL ? (uint)0 : Convert.ToUInt32(value.N);
        }

        public static object NToNullableUInt32(AttributeValue value)
        {
            return value.NULL ? null : (uint?)Convert.ToUInt32(value.N);
        }

        public static object NToUInt64(AttributeValue value)
        {
            return value.NULL ? (ulong)0 : Convert.ToUInt64(value.N);
        }

        public static object NToNullableUInt64(AttributeValue value)
        {
            return value.NULL ? null : (ulong?)Convert.ToUInt64(value.N);
        }

        public static object NToDecimal(AttributeValue value)
        {
            return value.NULL ? (decimal)0 : Convert.ToDecimal(value.N);
        }

        public static object NToNullableDecimal(AttributeValue value)
        {
            return value.NULL ? null : (decimal?)Convert.ToDecimal(value.N);
        }

        public static object NToDouble(AttributeValue value)
        {
            return value.NULL ? (double)0 : Convert.ToDouble(value.N);
        }

        public static object NToNullableDouble(AttributeValue value)
        {
            return value.NULL ? null : (double?)Convert.ToDouble(value.N);
        }

        public static object NToFloat(AttributeValue value)
        {
            return value.NULL ? (float)0 : Convert.ToSingle(value.N);
        }

        public static object NToNullableFloat(AttributeValue value)
        {
            return value.NULL ? null : (float?)Convert.ToSingle(value.N);
        }

        public static object BoolToBool(AttributeValue value)
        {
            return !value.NULL && value.BOOL;
        }

        public static object BoolToNullableBool(AttributeValue value)
        {
            return value.NULL ? null : (bool?)value.BOOL;
        }

        #endregion

        #region FromDotNetToDynamoMethods

        public static AttributeValue StringToS(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { S = (string)value };
        }

        public static AttributeValue CharToS(object value)
        {
            return new AttributeValue { S = ((char)value).ToString() };
        }

        public static AttributeValue NullableCharToS(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { S = ((char)value).ToString() };
        }

        public static AttributeValue DateTimeToS(object value)
        {
            return new AttributeValue { S = ((DateTime)value).ToUniversalTime().ToString("o") };
        }

        public static AttributeValue NullableDateTimeToS(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { S = ((DateTime)value).ToUniversalTime().ToString("o") };
        }

        public static AttributeValue ByteToN(object value)
        {
            return new AttributeValue { N = ((byte)value).ToString() };
        }

        public static AttributeValue NullableByteToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((byte)value).ToString() };
        }

        public static AttributeValue DecimalToN(object value)
        {
            return new AttributeValue { N = ((decimal)value).ToString() };
        }

        public static AttributeValue NullableDecimalToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((decimal)value).ToString() };
        }

        public static AttributeValue DoubleToN(object value)
        {
            return new AttributeValue { N = ((double)value).ToString() };
        }

        public static AttributeValue NullableDoubleToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((double)value).ToString() };
        }

        public static AttributeValue FloatToN(object value)
        {
            return new AttributeValue { N = ((float)value).ToString() };
        }

        public static AttributeValue NullableFloatToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((float)value).ToString() };
        }

        public static AttributeValue Int32ToN(object value)
        {
            return new AttributeValue { N = ((int)value).ToString() };
        }

        public static AttributeValue NullableInt32ToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((int)value).ToString() };
        }

        public static AttributeValue Int64ToN(object value)
        {
            return new AttributeValue { N = ((long)value).ToString() };
        }

        public static AttributeValue NullableInt64ToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((long)value).ToString() };
        }

        public static AttributeValue UInt32ToN(object value)
        {
            return new AttributeValue { N = ((uint)value).ToString() };
        }

        public static AttributeValue NullableUInt32ToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((uint)value).ToString() };
        }

        public static AttributeValue UInt64ToN(object value)
        {
            return new AttributeValue { N = ((ulong)value).ToString() };
        }

        public static AttributeValue NullableUInt64ToN(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { N = ((ulong)value).ToString() };
        }

        public static AttributeValue BoolToBool(object value)
        {
            return new AttributeValue { BOOL = (bool)value };
        }

        public static AttributeValue NullableBoolToBool(object value)
        {
            return value == null
                ? new AttributeValue { NULL = true }
                : new AttributeValue { BOOL = (bool)value };
        }

        // ToDo: Add conversions for ICollection if we want to start storing sets (Dynamo only support sets which implement ICollection)
        #endregion
    }
}