﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Amazon.DynamoDBv2.Model;
using DynamoDbRepo.Model;

namespace DynamoDbRepo.Helper
{
    public static class DataType
    {
        private static readonly List<TypeMap> TypeMappings = new List<TypeMap>
        {
            new TypeMap (typeof(string), "S", TypeConvert.SToString, TypeConvert.StringToS ),
            new TypeMap (typeof(char), "S", TypeConvert.SToChar, TypeConvert.CharToS ),
            new TypeMap (typeof(char?), "S", TypeConvert.SToNullableChar, TypeConvert.NullableCharToS ),
            new TypeMap (typeof(DateTime), "S", TypeConvert.SToDateTime, TypeConvert.DateTimeToS ),
            new TypeMap (typeof(DateTime?), "S", TypeConvert.SToNullableDateTime, TypeConvert.NullableDateTimeToS ),
            new TypeMap (typeof(byte), "N", TypeConvert.NToByte, TypeConvert.ByteToN ),
            new TypeMap (typeof(byte?), "N", TypeConvert.NToNullableByte, TypeConvert.NullableByteToN ),
            new TypeMap (typeof(int), "N", TypeConvert.NToInt32, TypeConvert.Int32ToN ),
            new TypeMap (typeof(int?), "N", TypeConvert.NToNullableInt32, TypeConvert.NullableInt32ToN ),
            new TypeMap (typeof(long), "N", TypeConvert.NToInt64, TypeConvert.Int64ToN ),
            new TypeMap (typeof(long?), "N", TypeConvert.NToNullableInt64, TypeConvert.NullableInt64ToN ),
            new TypeMap (typeof(uint), "N", TypeConvert.NToUInt32, TypeConvert.UInt32ToN ),
            new TypeMap (typeof(uint?), "N", TypeConvert.NToNullableUInt32, TypeConvert.NullableUInt32ToN ),
            new TypeMap (typeof(ulong), "N", TypeConvert.NToUInt64, TypeConvert.UInt64ToN ),
            new TypeMap (typeof(ulong?), "N", TypeConvert.NToNullableUInt64, TypeConvert.NullableUInt64ToN ),
            new TypeMap (typeof(decimal), "N", TypeConvert.NToDecimal, TypeConvert.DecimalToN ),
            new TypeMap (typeof(decimal?), "N", TypeConvert.NToNullableDecimal, TypeConvert.NullableDecimalToN ),
            new TypeMap (typeof(double), "N", TypeConvert.NToDouble, TypeConvert.DoubleToN ),
            new TypeMap (typeof(double?), "N", TypeConvert.NToNullableDouble, TypeConvert.NullableDoubleToN ),
            new TypeMap (typeof(float), "N", TypeConvert.NToFloat, TypeConvert.FloatToN ),
            new TypeMap (typeof(float?), "N", TypeConvert.NToNullableFloat, TypeConvert.NullableFloatToN ),
            new TypeMap (typeof(bool), "BOOL", TypeConvert.BoolToBool, TypeConvert.BoolToBool ),
            new TypeMap (typeof(bool?), "BOOL", TypeConvert.NullableBoolToBool, TypeConvert.NullableBoolToBool )
        };

        public static AttributeValue GetAttributeValueFromObject(PropertyInfo keyPropertyInfo, object value)
        {
            return GetAttributeValueFromObject(keyPropertyInfo.PropertyType, value);
        }

        public static AttributeValue GetAttributeValueFromObject(Type objectType, object value)
        {
            TypeMap mapToUse = GetTypeMap(objectType);
            return mapToUse.ToDynamoAttributeValue.Invoke(value);
        }

        public static TypeMap GetTypeMap(Type propertyType)
        {
            if (propertyType.IsEnum)
                propertyType = typeof(byte);

            TypeMap map = TypeMappings.FirstOrDefault(x => x.DotNetType == propertyType);
            if (map == null)
                throw new NotSupportedException($"No mapping has been defined for object type {propertyType}. Please add a new mapping to the {nameof(TypeMappings)} list in the {nameof(DataType)} class");

            return map;
        }
    }
}