﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Expression = System.Linq.Expressions.Expression;

namespace DynamoDbRepo.Helper
{
    public static class ExpressionToScan
    {
        public static ICollection<ScanCondition> Translate<T>(Expression<Func<T, bool>> predicate)
        {
            return TranslateExpression<T>(predicate.Body);
        }

        private static ICollection<ScanCondition> TranslateExpression<T>(Expression expression)
        {
            switch (expression.NodeType)
            {
                case ExpressionType.AndAlso:
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                    BinaryExpression binaryExpression = (BinaryExpression)expression;
                    return TranslateBinaryExpression<T>(binaryExpression.NodeType, binaryExpression.Left, binaryExpression.Right);
                case ExpressionType.Call:
                    return TranslateMethodCallExpression<T>((MethodCallExpression)expression);
                case ExpressionType.Not:
                    return TranslateNotExpression<T>((UnaryExpression)expression);
                default:
                    throw new NotSupportedException($"No translation exists for the {expression.NodeType} expression type.");
            }
        }

        private static ICollection<ScanCondition> TranslateNotExpression<T>(UnaryExpression expression)
        {
            ICollection<ScanCondition> preNotResult = TranslateExpression<T>(expression.Operand);
            if (preNotResult.Count > 1)
                throw new NotSupportedException("Not operation translations only support one Operand e.g. !x.Foo.Contains(\"foo\") or !(\"x.Foo == \"foo\")");

            ScanCondition result = InvertScanConditionComparer(preNotResult.First());
            return new List<ScanCondition> { result };
        }

        private static ICollection<ScanCondition> TranslateBinaryExpression<T>(ExpressionType comparer, Expression left, Expression right)
        {
            if (!IsComparerSupported(comparer))
                throw new NotSupportedException($"{comparer} comparer is not yet translatable into a dynamo scan");

            if (comparer == ExpressionType.AndAlso)
            {
                var andAlsoLeft = TranslateExpression<T>(left);
                var andAlsoRight = TranslateExpression<T>(right);
                return andAlsoLeft.Union(andAlsoRight).ToList();
            }

            bool leftIsProperty = IsPropertyOf<T>(left);
            bool rightIsProperty = IsPropertyOf<T>(right);
            if (!(leftIsProperty ^ rightIsProperty))
                throw new NotSupportedException("Must have exactly one property to compare in Dynamo. This is mostly likely an attempt to compare two field which both live in Dynamo");

            string propertyName = GetPropertyName(leftIsProperty ? (MemberExpression)left : (MemberExpression)right);
            object compareTo = ExpressionHelper.GetReturnValue(leftIsProperty ? right : left);

            switch (comparer)
            {
                case ExpressionType.Equal:
                    return compareTo == null
                        ? new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.IsNull) }
                        : new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.Equal, compareTo) };
                case ExpressionType.NotEqual:
                    return compareTo == null
                        ? new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.IsNotNull) }
                        : new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.NotEqual, compareTo) };
                case ExpressionType.GreaterThan:
                    return new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.GreaterThan, compareTo) };
                case ExpressionType.GreaterThanOrEqual:
                    return new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.GreaterThanOrEqual, compareTo) };
                case ExpressionType.LessThan:
                    return new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.LessThan, compareTo) };
                case ExpressionType.LessThanOrEqual:
                    return new List<ScanCondition> { new ScanCondition(propertyName, ScanOperator.LessThanOrEqual, compareTo) };
            }

            throw new Exception("Unexpectly reached the end of the method without returning a value as expected");
        }

        private static ICollection<ScanCondition> TranslateMethodCallExpression<T>(MethodCallExpression expression)
        {
            MemberExpression objectMember = expression.Object as MemberExpression;
            if (objectMember == null || !IsPropertyOf<T>(objectMember.Expression))
                throw new NotSupportedException("Method Calls such as .Contains and .StartsWith must access a member of the Dynamo Table being scanned");

            switch (expression.Method.Name)
            {
                case "Contains":
                    return new List<ScanCondition> { new ScanCondition(objectMember.Member.Name, ScanOperator.Contains, ExpressionHelper.GetReturnValue(expression.Arguments.First())) };
                case "StartsWith":
                    return new List<ScanCondition> { new ScanCondition(objectMember.Member.Name, ScanOperator.BeginsWith, ExpressionHelper.GetReturnValue(expression.Arguments.First())) };
                default:
                    // ToDo: Implement translation for methods such as .Equals() although this seems trivial for effort involved
                    throw new NotSupportedException($"No translation exists for Method call .{expression.Method.Name}");
            }
        }

        private static ScanCondition InvertScanConditionComparer(ScanCondition scanCondition)
        {
            switch (scanCondition.Operator)
            {
                case ScanOperator.Contains:
                    scanCondition.Operator = ScanOperator.NotContains;
                    return scanCondition;
                case ScanOperator.Equal:
                    scanCondition.Operator = ScanOperator.NotEqual;
                    return scanCondition;
                case ScanOperator.NotEqual:
                    scanCondition.Operator = ScanOperator.Equal;
                    return scanCondition;
                case ScanOperator.GreaterThan:
                    scanCondition.Operator = ScanOperator.LessThanOrEqual;
                    return scanCondition;
                case ScanOperator.GreaterThanOrEqual:
                    scanCondition.Operator = ScanOperator.LessThan;
                    return scanCondition;
                case ScanOperator.LessThan:
                    scanCondition.Operator = ScanOperator.GreaterThanOrEqual;
                    return scanCondition;
                case ScanOperator.LessThanOrEqual:
                    scanCondition.Operator = ScanOperator.GreaterThan;
                    return scanCondition;
                case ScanOperator.IsNull:
                    scanCondition.Operator = ScanOperator.IsNotNull;
                    return scanCondition;
                case ScanOperator.IsNotNull:
                    scanCondition.Operator = ScanOperator.IsNull;
                    return scanCondition;
                case ScanOperator.BeginsWith:
                    throw new NotSupportedException("Dynamo does not support an equivalent operation for does not begin with.");
                default:
                    throw new NotSupportedException($"No translation exists for inverting the {scanCondition.Operator} operation.");
            }
        }

        private static bool IsComparerSupported(ExpressionType comparer)
        {
            List<ExpressionType> supportedComparers = new List<ExpressionType>
            {
                ExpressionType.AndAlso,
                ExpressionType.Equal,
                ExpressionType.NotEqual,
                ExpressionType.GreaterThan,
                ExpressionType.GreaterThanOrEqual,
                ExpressionType.LessThan,
                ExpressionType.LessThanOrEqual
            };

            return supportedComparers.Any(x => x == comparer);
        }

        private static string GetPropertyName(MemberExpression expression)
        {
            return PropertyMetadata.GetAttributeName(expression.Expression.Type.GetProperty(expression.Member.Name));
        }

        private static bool IsPropertyOf<T>(Expression expression)
        {
            return expression.NodeType == ExpressionType.MemberAccess && ((MemberExpression)expression).Expression.Type == typeof(T)
                || expression.NodeType == ExpressionType.Parameter && ((ParameterExpression)expression).Type == typeof(T);
        }
    }
}