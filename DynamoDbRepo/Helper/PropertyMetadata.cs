﻿using System;
using System.Linq;
using System.Reflection;
using Amazon.DynamoDBv2.DataModel;

namespace DynamoDbRepo.Helper
{
    public static class PropertyMetadata
    {
        public static string GetAttributeName(PropertyInfo property)
        {
            CustomAttributeData propertyAttribute = property.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(DynamoDBPropertyAttribute));
            return IsCustomAttributeTypeOfString(propertyAttribute) ? propertyAttribute.ConstructorArguments[0].Value as string : property.Name;
        }

        public static string GetAttributeName<T>(string propertyName)
        {
            return GetAttributeName(typeof(T).GetProperty(propertyName));
        }

        public static PropertyInfo GetGlobalSecondaryIndexPartitionKeyProperty<T>(string indexName)
        {
            return GetGlobalSecondaryIndexPartitionKeyProperty(typeof(T), indexName);
        }

        public static PropertyInfo GetGlobalSecondaryIndexPartitionKeyProperty(Type type, string indexName)
        {
            PropertyInfo partitionKeyProperty = AttributeHelper.FindPropertiesWithAttribute(type, typeof(DynamoDBGlobalSecondaryIndexHashKeyAttribute))
                .FirstOrDefault(x => x.CustomAttributes.Any(y => y.ConstructorArguments.FirstOrDefault().Value as string == indexName));

            if (partitionKeyProperty == null)
                throw new NullReferenceException($"Unable to locate the {nameof(DynamoDBGlobalSecondaryIndexHashKeyAttribute)} attribue for {nameof(type)}");
            return partitionKeyProperty;
        }

        public static PropertyInfo GetGlobalSecondaryIndexSortKeyProperty<T>(string indexName)
        {
            return GetGlobalSecondaryIndexSortKeyProperty(typeof(T), indexName);
        }

        public static PropertyInfo GetGlobalSecondaryIndexSortKeyProperty(Type type, string indexName)
        {
            PropertyInfo sortKeyProperty = AttributeHelper.FindPropertiesWithAttribute(type, typeof(DynamoDBGlobalSecondaryIndexRangeKeyAttribute))
                .FirstOrDefault(x => x.CustomAttributes.Any(y => y.ConstructorArguments.FirstOrDefault().Value as string == indexName));

            if (sortKeyProperty == null)
                throw new NullReferenceException($"Unable to locate the {nameof(DynamoDBGlobalSecondaryIndexRangeKeyAttribute)} attribue for {nameof(type)}");
            return sortKeyProperty;
        }

        public static string GetGlobalSecondaryIndexName<T>()
        {
            return GetGlobalSecondaryIndexName(typeof(T));
        }

        public static string GetGlobalSecondaryIndexName(Type type)
        {
            var propertyWithSecondaryIndexHashKey = type.GetProperties().Where(x => x.CustomAttributes.Any(y => y.AttributeType == typeof(DynamoDBGlobalSecondaryIndexHashKeyAttribute))).ToList();
            if (!propertyWithSecondaryIndexHashKey.Any())
                throw new ArgumentException($"Cannot locate any properties in {nameof(type)} with the {nameof(DynamoDBGlobalSecondaryIndexHashKeyAttribute)} attribute");

            if (propertyWithSecondaryIndexHashKey.Count > 1)
                throw new ArgumentException($"Found more than one property in {nameof(type)} with the {nameof(DynamoDBGlobalSecondaryIndexHashKeyAttribute)} attribute. Please use a less ambiguous method when more than one Secondary Index exists.");

            var propertyWithAttribute = propertyWithSecondaryIndexHashKey.First().CustomAttributes.FirstOrDefault(x =>
                x.AttributeType == typeof(DynamoDBGlobalSecondaryIndexHashKeyAttribute)
                || x.AttributeType == typeof(DynamoDBGlobalSecondaryIndexRangeKeyAttribute));

            return IsCustomAttributeTypeOfString(propertyWithAttribute) ? propertyWithAttribute.ConstructorArguments[0].Value as string : null;
        }

        public static string GetGlobalSecondaryIndexName(PropertyInfo property)
        {
            CustomAttributeData propertyAttribute = property.CustomAttributes.FirstOrDefault(x =>
                x.AttributeType == typeof(DynamoDBGlobalSecondaryIndexHashKeyAttribute)
                || x.AttributeType == typeof(DynamoDBGlobalSecondaryIndexRangeKeyAttribute));

            return IsCustomAttributeTypeOfString(propertyAttribute) ? propertyAttribute.ConstructorArguments[0].Value as string : null;
        }

        public static string GetTableName<T>()
        {
            return GetTableName(typeof(T));
        }

        public static string GetTableName(Type tableClass)
        {
            CustomAttributeData propertyAttribute = tableClass.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(DynamoDBTableAttribute));
            if (propertyAttribute == null)
                throw new ArgumentException($"Unable to locate the {nameof(DynamoDBTableAttribute)} attribute on class {nameof(tableClass)}");

            return IsCustomAttributeTypeOfString(propertyAttribute) ? propertyAttribute.ConstructorArguments[0].Value as string : tableClass.Name;
        }

        public static bool IsIgnored(PropertyInfo propertyInfo)
        {
            return propertyInfo.CustomAttributes.Any(x => x.AttributeType == typeof(DynamoDBIgnoreAttribute));
        }

        private static bool IsCustomAttributeTypeOfString(CustomAttributeData attribute)
        {
            return attribute != null && attribute.ConstructorArguments.Any() && attribute.ConstructorArguments[0].ArgumentType == typeof(string);
        }
    }
}