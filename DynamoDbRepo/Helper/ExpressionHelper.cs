﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DynamoDbRepo.Helper
{
    public static class ExpressionHelper
    {
        /// <summary>
        /// Gets an array with the resolved parameters using <see cref="GetReturnValue"/> while avoiding compiling the expression wherever possible
        /// </summary>
        /// <param name="expression">The method expression of which you want to find parameter values</param>
        /// <returns>array of objects representing the methods resolved parameters</returns>
        public static object[] GetMethodParameters(MethodCallExpression expression)
        {
            int paramCount = expression.Arguments.Count;
            object[] param = new object[paramCount];

            for (int i = 0; i < paramCount; i++)
            {
                param[i] = ExpressionHelper.GetReturnValue(expression.Arguments[i]);
            }

            return param;
        }

        /// <summary>
        /// Gets the return value of an expression using reflection wherever possible and without invoking or compiling the expression if possible.
        /// </summary>
        /// <param name="expression">The expression of which you want to find it's return value</param>
        /// <returns>the return value of said expression</returns>
        public static object GetReturnValue(Expression expression)
        {
            bool attemptedWithoutGenericMethod = true;
            try
            {
                switch (expression.NodeType)
                {
                    case ExpressionType.Constant:
                        return ((ConstantExpression)expression).Value;
                    case ExpressionType.MemberAccess:
                        return GetMemberExpressionValue((MemberExpression)expression);
                    case ExpressionType.Call:
                        return GetMethodCallExpressionValue((MethodCallExpression)expression);
                    case ExpressionType.New:
                        NewExpression newExpression = (NewExpression) expression;
                        object[] values = newExpression.Arguments.Select(GetReturnValue).ToArray();
                        return newExpression.Constructor.Invoke(values);
                    default:
                        attemptedWithoutGenericMethod = false;
                        return GetGenericExpressionValue(expression);
                }
            }
            catch
            {
                // if we got an exception without using the generic lambda method, then try using the lambda instead.
                return attemptedWithoutGenericMethod ? GetGenericExpressionValue(expression) : null;
            }
        }

        /// <summary>
        /// Gets the return value of a method call expression by resolving it's parameters and invoking the method.
        /// </summary>
        /// <param name="expression">The expression of which you want to find it's return value</param>
        /// <returns>the return value of said expression</returns>
        private static object GetMethodCallExpressionValue(MethodCallExpression expression)
        {
            var method = expression.Method;
            object resolvedExpressionObject = expression.Object == null ? null : ExpressionHelper.GetReturnValue(expression.Object);
            return method.Invoke(resolvedExpressionObject, ExpressionHelper.GetMethodParameters(expression));
        }

        /// <summary>
        /// Gets the return value of a Member Expression by recursively iterating through child expressions to find the root and then resolving based on the root expression node type and member type.
        /// Currently works for Member types of Field or Property before falling back to resolving via a generic lambda compilation.
        /// </summary>
        /// <param name="memberExpression">Top of tree expression to retreive value for</param>
        /// <param name="parentMemberExpression">Partents expression if recursively iterating through the tree looking for the root.</param>
        /// <returns></returns>
        private static object GetMemberExpressionValue(MemberExpression memberExpression, MemberExpression parentMemberExpression = null)
        {
            if (memberExpression.Expression != null && memberExpression.Expression.NodeType != ExpressionType.Constant)
            {
                if (!(memberExpression.Expression is MemberExpression)) return ExpressionHelper.GetGenericExpressionValue(memberExpression); // Should never happen, but if the inner expression is not a member expression or constant expression then use generic method to compile return value.
                return ExpressionHelper.GetMemberExpressionValue((MemberExpression)memberExpression.Expression, (MemberExpression)memberExpression); // Recursive loop until we get to the root.
            }

            // get child value
            object memberValue;
            switch (memberExpression.Member.MemberType)
            {
                case MemberTypes.Field:
                    var constExp = (ConstantExpression)memberExpression.Expression;
                    memberValue = ((FieldInfo)memberExpression.Member).GetValue(constExp?.Value);
                    break;
                case MemberTypes.Property:
                    memberValue = ((PropertyInfo)memberExpression.Member).GetValue(memberExpression.Member);
                    break;
                default:
                    return GetGenericExpressionValue(memberExpression);
            };

            if (parentMemberExpression == null) return memberValue;

            // get parent value
            switch (parentMemberExpression.Member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)parentMemberExpression.Member).GetValue(memberValue);
                case MemberTypes.Property:
                    return ((PropertyInfo)parentMemberExpression.Member).GetValue(memberValue);
                default:
                    return GetGenericExpressionValue(memberExpression);
            };
        }

        /// <summary>
        /// Should work in most cases, however the more specific methods will operate much faster without compiling the expression. (tested 100,000 loops = 10 seconds(fast) vs 0.5 seconds(lightning))
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static object GetGenericExpressionValue(Expression expression)
        {
            Expression<Func<object>> getterLambda = Expression.Lambda<Func<object>>(expression);
            Func<object> getter = getterLambda.Compile();
            return getter();
        }
    }
}
