﻿using System;
using Amazon.DynamoDBv2.DocumentModel;

namespace DynamoDbRepo.Helper
{
    public static class QueryHelper
    {
        public static string HighLevelOperatorToLowLevelOperator(QueryOperator queryOperator)
        {
            // See http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/QueryAndScan.html for list of query operators
            switch (queryOperator)
            {
                case QueryOperator.Equal:
                    return "=";
                case QueryOperator.LessThanOrEqual:
                    return "<=";
                case QueryOperator.LessThan:
                    return "<";
                case QueryOperator.GreaterThanOrEqual:
                    return ">=";
                case QueryOperator.GreaterThan:
                    return ">";
                case QueryOperator.BeginsWith:
                    return "begins_with";
                case QueryOperator.Between:
                    return "BETWEEN";
                default:
                    throw new ArgumentOutOfRangeException(nameof(queryOperator), queryOperator, null);
            }
        }
    }
}